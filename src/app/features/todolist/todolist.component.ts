import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/common/todo';
import { ToastrService } from 'ngx-toastr';
import { Statistic } from 'src/app/common/statistic';
import { Store } from '@ngrx/store';
import {
  addTodo,
  deleteAllTodo,
  deleteTodo,
  getTodo,
  getTodoSuccess,
  getTodos,
  updateTodo,
} from 'src/app/core/store/todo/todo.action';
import {
  statisticSelector,
  todoListSelector,
  todoSelector,
  todoStatusMessageSelector,
} from 'src/app/core/store/todo/todo.selector';
import { AppState } from 'src/app/core/store/app.state';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css'],
})
export class TodolistComponent implements OnInit {
  todoList: Todo[] = [];
  todoInput: string = '';
  todoChoosed: Todo = { id: 0, name: '', status: 0 };
  statistic: Statistic = { success: 0, total: 0 };
  scaleXValue: number = 0;
  constructor(private toastr: ToastrService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(getTodos());

    this.store
      .select(todoListSelector)
      .subscribe((todos) => (this.todoList = todos));
    this.store.select(statisticSelector).subscribe((statistic) => {
      this.statistic = {
        ...this.statistic,
        total: statistic.total,
        success: statistic.success,
      };
      this.scaleXValue = Math.min(statistic.success / statistic.total, 1);
    });

    this.store.select(todoSelector).subscribe((todo) => {
      this.todoChoosed = todo;
      this.todoInput = todo.name;
    });

    this.store.select(todoStatusMessageSelector).subscribe((statusMessage) => {
      if (
        statusMessage.status == 'add_success' ||
        statusMessage.status == 'update_success' ||
        statusMessage.status == 'delete_success' ||
        statusMessage.status == 'delete_all_success'
      ) {
        this.toastr.success(statusMessage.message);
      } else if (
        statusMessage.status == 'add_failed' ||
        statusMessage.status == 'update_failed' ||
        statusMessage.status == 'delete_failed' ||
        statusMessage.status == 'delete_all_failed'
      ) {
        this.toastr.error(statusMessage.message);
      }
    });
  }

  handleChangeInput(even: any) {
    this.todoInput = even.target.value;
  }

  handleChooseTodo(id: number) {
    this.store.dispatch(getTodo({ payload: id }));
    const todo = this.todoList.find((todo) => todo.id == id);
    if (todo) {
      this.store.dispatch(getTodoSuccess({ payload: todo }));
    } else {
      this.toastr.error('Error');
    }
  }

  handleAddTodo(name: string) {
    this.store.dispatch(addTodo({ payload: name }));
  }

  handleChangeStatus(id: number) {
    let todo = this.todoList.find((todo) => todo.id == id);
    if (todo) {
      if (todo.status == 1) todo = { ...todo, status: 0 };
      else todo = { ...todo, status: 1 };
      this.store.dispatch(updateTodo({ payload: todo }));
    } else {
      this.toastr.error('Error');
    }
    this.store
      .select(todoListSelector)
      .subscribe((todos) => (this.todoList = todos));
  }

  handleUpdateName(id: number, name: string) {
    let todo = this.todoList.find((todo) => todo.id == id);
    if (todo) {
      todo = { ...todo, name };
      this.store.dispatch(updateTodo({ payload: todo }));
    } else {
      this.toastr.error('Error');
    }
    this.store
      .select(todoListSelector)
      .subscribe((todos) => (this.todoList = todos));
  }

  handleDelete(id: number) {
    if (confirm('Do you want delete this todo')) {
      this.store.dispatch(deleteTodo({ payload: id }));
    }
  }

  handleDeleteAll() {
    if (confirm('Do you want delete this todo?')) {
      this.store.dispatch(deleteAllTodo());
    }
  }
}
