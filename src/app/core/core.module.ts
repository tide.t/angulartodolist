import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { todoReducer } from './store/todo/todo.reducer';
import { EffectsModule } from '@ngrx/effects';
import { TodosEffects } from './store/todo/todo.effect';

@NgModule({
  imports: [
    StoreModule.forFeature('todo', todoReducer),
    EffectsModule.forFeature([TodosEffects]),
  ],
})
export class CoreModule {}
