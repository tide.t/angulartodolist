import { TodoState } from './todo/todo.state';

export interface AppState {
  feature_todo: TodoState;
}
