import { Todo } from 'src/app/common/todo';

export interface TodoState {
  id: number;
  todos: Todo[];
  todo: Todo;
  message: string;
  status:
    | 'pending'
    | 'loading'
    | 'failed'
    | 'success'
    | 'add_success'
    | 'add_failed'
    | 'update_success'
    | 'update_failed'
    | 'delete_success'
    | 'delete_failed'
    | 'delete_all_success'
    | 'delete_all_failed';

  statistic: {
    success: number;
    total: number;
  };
}
