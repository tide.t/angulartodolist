import { TodoState } from './todo.state';
import * as TodoActions from './todo.action';

const initialState: TodoState = {
  id: 0,
  todos: [],
  todo: { id: 0, name: '', status: 0 },
  message: '',
  status: 'pending',
  statistic: { success: 0, total: 0 },
};

export function todoReducer(
  state: TodoState = initialState,
  action: TodoActions.TodoActions
): TodoState {
  switch (action.type) {
    case TodoActions.GET_TODOS:
      return { ...state, status: 'loading', message: '' };

    case TodoActions.GET_TODOS_SUCCESS: {
      const total = action.payload.length;
      const success = action.payload.reduce((x, todo) => {
        if (todo.status == 0) {
          return x + 1;
        } else return x;
      }, 0);

      return {
        ...state,
        status: 'success',
        todos: action.payload,
        statistic: { ...state.statistic, success, total },
      };
    }

    case TodoActions.GET_TODOS_FAILED:
      return {
        ...state,
        status: 'failed',
        todos: [],
        statistic: initialState.statistic,
        message: action.payload,
      };

    case TodoActions.GET_TODO:
      return { ...state, status: 'loading' };

    case TodoActions.GET_TODO_SUCCESS:
      return {
        ...state,
        status: 'success',
        todo: {
          id: action.payload.id,
          name: action.payload.name,
          status: action.payload.status,
        },
      };

    case TodoActions.GET_TODO_FAILED:
      return {
        ...state,
        status: 'failed',
        todo: initialState.todo,
        message: action.payload,
      };

    case TodoActions.ADD:
      return { ...state, status: 'loading', message: '' };

    case TodoActions.ADD_SUCCESS:
      return {
        ...state,
        status: 'add_success',
        message: action.payload,
        todos: [...state.todos, action.todo],
        statistic: { ...state.statistic, total: state.statistic.total + 1 },
      };

    case TodoActions.ADD_FAILED:
      return { ...state, status: 'add_failed', message: action.payload };

    case TodoActions.UPDATE: {
      return {
        ...state,
        status: 'loading',
        todo: {
          id: action.payload.id,
          name: action.payload.name,
          status: action.payload.status,
        },
      };
    }

    case TodoActions.UPDATE_SUCCESS: {
      const todoUpdate = state.todo;
      const todoList = state.todos.filter((todo) => todo.id != todoUpdate.id);
      todoList.push(todoUpdate);
      const success = todoList.reduce((x, todo) => {
        if (todo.status == 0) {
          return x + 1;
        } else return x;
      }, 0);
      return {
        ...state,
        status: 'update_success',
        message: action.payload,
        todos: todoList,
        statistic: { ...state.statistic, success },
      };
    }

    case TodoActions.UPDATE_FAILED: {
      state.status = 'update_failed';
      state.message = action.payload;
      return state;
    }

    case TodoActions.DELETE: {
      return { ...state, status: 'loading', id: action.payload };
    }

    case TodoActions.DELETE_SUCCESS: {
      const todoList = state.todos.filter((todo) => todo.id != state.id);
      const total = todoList.length;
      const success = todoList.reduce((x, todo) => {
        if (todo.status == 0) {
          return x + 1;
        } else return x;
      }, 0);
      return {
        ...state,
        status: 'delete_success',
        message: action.payload,
        todos: todoList,
        statistic: { success, total },
        id: 0,
      };
    }

    case TodoActions.DELETE_FAILED: {
      return { ...state, status: 'delete_failed', message: action.payload };
    }

    case TodoActions.DELETE_ALL:
      return {
        ...state,
        status: 'loading',
      };

    case TodoActions.DELETE_ALL_SUCCESS:
      return {
        ...state,
        id: initialState.id,
        todo: initialState.todo,
        todos: initialState.todos,
        message: action.payload,
        status: 'delete_all_success',
        statistic: initialState.statistic,
      };

    case TodoActions.DELETE_ALL_FAILED:
      return { ...state, status: 'delete_all_failed', message: action.payload };
  }
}
