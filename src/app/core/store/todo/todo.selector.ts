import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TodoState } from './todo.state';

const featuresTodo = createFeatureSelector<TodoState>('todo');

export const todoListSelector = createSelector(featuresTodo, (state) => {
  return state.todos;
});

export const statisticSelector = createSelector(
  featuresTodo,
  (state) => state.statistic
);
export const todoSelector = createSelector(featuresTodo, (state) => state.todo);

export const todoMessageSelector = createSelector(
  featuresTodo,
  (state) => state.message
);
export const todoStatusSelector = createSelector(
  featuresTodo,
  (state) => state.status
);

export const todoStatusMessageSelector = createSelector(
  featuresTodo,
  (state) => ({ status: state.status, message: state.message })
);
