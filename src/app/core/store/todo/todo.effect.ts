import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import {
  map,
  exhaustMap,
  catchError,
  mergeMap,
  switchMap,
} from 'rxjs/operators';
import { TodoService } from '../../services/todoService/todo.service';
import * as todoActions from './todo.action';
import { TodoResult } from 'src/app/common/todoResult';

@Injectable()
export class TodosEffects {
  getAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.getTodos),
      mergeMap(() => this.todoService.getAll()),
      map((todos) => todoActions.getTodosSuccess({ payload: todos })),
      catchError((error) => of(todoActions.getTodosFailed({ payload: error })))
    )
  );
  add$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.addTodo),
      map((action) => action.payload),
      mergeMap((name) => {
        return this.todoService.add(name);
      }),
      map((result: TodoResult) => {
        if (result.status == 1) {
          return todoActions.addTodoSuccess({
            payload: result.statusDescription,
            todo: result.todo,
          });
        } else {
          return todoActions.addTodoFailed({
            payload: result.statusDescription,
          });
        }
      }),
      catchError((error) => of(todoActions.addTodoFailed({ payload: error })))
    )
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.updateTodo),
      map((action) => action.payload),
      mergeMap((todo) => {
        return this.todoService.update(todo);
      }),
      map((result: TodoResult) => {
        if (result.status == 1) {
          return todoActions.updateTodoSuccess({
            payload: result.statusDescription,
          });
        } else {
          return todoActions.updateTodoFailed({
            payload: result.statusDescription,
          });
        }
      }),
      catchError((error) =>
        of(todoActions.updateTodoFailed({ payload: error }))
      )
    )
  );

  delete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.deleteTodo),
      map((action) => action.payload),
      mergeMap((id) => this.todoService.delete(id)),
      map((result: TodoResult) => {
        if (result.status == 1) {
          return todoActions.deleteTodoSuccess({
            payload: result.statusDescription,
          });
        } else {
          return todoActions.deleteTodoFailed({
            payload: result.statusDescription,
          });
        }
      }),
      catchError((error) =>
        of(todoActions.deleteTodoFailed({ payload: error }))
      )
    )
  );

  delete_all$ = createEffect(() =>
    this.actions$.pipe(
      ofType(todoActions.deleteAllTodo),
      mergeMap(() => this.todoService.delete_all()),
      map((result: TodoResult) => {
        if (result.status == 1) {
          return todoActions.deleteAllTodoSuccess({
            payload: result.statusDescription,
          });
        } else {
          return todoActions.deleteAllTodoFailed({
            payload: result.statusDescription,
          });
        }
      }),
      catchError((error) =>
        of(todoActions.deleteAllTodoFailed({ payload: error }))
      )
    )
  );
  constructor(private actions$: Actions, private todoService: TodoService) {}
}
