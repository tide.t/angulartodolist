import { ActionType, createAction, props } from '@ngrx/store';
import { Todo } from 'src/app/common/todo';

export const GET_TODOS = 'todo/get_todos';
export const GET_TODOS_SUCCESS = 'todo/get_todos_success';
export const GET_TODOS_FAILED = 'todo/get_todos_failed';

export const GET_TODO = 'todo/get_todo';
export const GET_TODO_SUCCESS = 'todo/get_todo_success';
export const GET_TODO_FAILED = 'todo/get_todo_failed';

export const ADD = 'todo/add';
export const ADD_SUCCESS = 'todo/add_success';
export const ADD_FAILED = 'todo/add_failed';

export const UPDATE = 'todo/update';
export const UPDATE_SUCCESS = 'todo/update_success';
export const UPDATE_FAILED = 'todo/update_failed';

export const DELETE = 'todo/delete';
export const DELETE_SUCCESS = 'todo/delete_success';
export const DELETE_FAILED = 'todo/delete_failed';

export const DELETE_ALL = 'todo/delete_all';
export const DELETE_ALL_SUCCESS = 'todo/delete_all_success';
export const DELETE_ALL_FAILED = 'todo/delete_all_failed';

export const getTodos = createAction(GET_TODOS);
export const getTodosSuccess = createAction(
  GET_TODOS_SUCCESS,
  props<{ payload: Todo[] }>()
);
export const getTodosFailed = createAction(
  GET_TODOS_FAILED,
  props<{ payload: string }>()
);

export const getTodo = createAction(GET_TODO, props<{ payload: number }>());
export const getTodoSuccess = createAction(
  GET_TODO_SUCCESS,
  props<{ payload: Todo }>()
);
export const getTodoFailed = createAction(
  GET_TODO_FAILED,
  props<{ payload: string }>()
);

export const addTodo = createAction(ADD, props<{ payload: string }>());
export const addTodoSuccess = createAction(
  ADD_SUCCESS,
  props<{ payload: string; todo: Todo }>()
);
export const addTodoFailed = createAction(
  ADD_FAILED,
  props<{ payload: string }>()
);

export const updateTodo = createAction(UPDATE, props<{ payload: Todo }>());
export const updateTodoSuccess = createAction(
  UPDATE_SUCCESS,
  props<{ payload: string }>()
);
export const updateTodoFailed = createAction(
  UPDATE_FAILED,
  props<{ payload: string }>()
);

export const deleteTodo = createAction(DELETE, props<{ payload: number }>());
export const deleteTodoSuccess = createAction(
  DELETE_SUCCESS,
  props<{ payload: string }>()
);
export const deleteTodoFailed = createAction(
  DELETE_FAILED,
  props<{ payload: string }>()
);

export const deleteAllTodo = createAction(DELETE_ALL);
export const deleteAllTodoSuccess = createAction(
  DELETE_ALL_SUCCESS,
  props<{ payload: string }>()
);
export const deleteAllTodoFailed = createAction(
  DELETE_ALL_FAILED,
  props<{ payload: string }>()
);

export type TodoActions =
  | ActionType<typeof getTodos>
  | ActionType<typeof getTodosSuccess>
  | ActionType<typeof getTodosFailed>
  | ActionType<typeof getTodo>
  | ActionType<typeof getTodoSuccess>
  | ActionType<typeof getTodoFailed>
  | ActionType<typeof addTodo>
  | ActionType<typeof addTodoSuccess>
  | ActionType<typeof addTodoFailed>
  | ActionType<typeof updateTodo>
  | ActionType<typeof updateTodoSuccess>
  | ActionType<typeof updateTodoFailed>
  | ActionType<typeof deleteTodo>
  | ActionType<typeof deleteTodoSuccess>
  | ActionType<typeof deleteTodoFailed>
  | ActionType<typeof deleteAllTodo>
  | ActionType<typeof deleteAllTodoSuccess>
  | ActionType<typeof deleteAllTodoFailed>;
