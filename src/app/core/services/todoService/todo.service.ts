import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../../../common/todo';
@Injectable({
  providedIn: 'root',
})
export class TodoService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Todo[]> {
    return this.http.get<any>('http://localhost:8081/todo/');
  }

  add(name: string): Observable<any> {
    return this.http.post<any>('http://localhost:8081/todo/add', { name });
  }

  update(todo: Todo): Observable<any> {
    return this.http.put<any>('http://localhost:8081/todo/update', todo);
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(`http://localhost:8081/todo/delete?id=${id}`);
  }

  statistic(): Observable<any> {
    return this.http.get<any>('http://localhost:8081/todo/statistic');
  }

  delete_all(): Observable<any> {
    return this.http.delete<any>('http://localhost:8081/todo/delete-all');
  }
}
