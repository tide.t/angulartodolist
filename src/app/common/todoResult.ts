import { Todo } from './todo';

export class TodoResult {
  status: number;
  statusDescription: string;
  todo: Todo;

  constructor(status: number, statusDescription: string, todo: Todo) {
    this.status = status;
    this.statusDescription = statusDescription;
    this.todo = todo;
  }
}
