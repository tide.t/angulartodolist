export class Statistic {
  success: number;
  total: number;
  constructor(success: number, total: number) {
    this.success = success;
    this.total = total;
  }
}
